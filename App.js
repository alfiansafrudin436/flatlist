import React , {useState} from 'react';
import { Text, View, StyleSheet,FlatList, Image } from 'react-native';
import Constants from 'expo-constants';

export default function App() {

  const dataMahasiswa=[
    {
      id:1,
      nama:'Alfian',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:2,
      nama:'Tasya',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:3,
      nama:'Syifa',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:1,
      nama:'Salma',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:4,
      nama:'Andy',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:5,
      nama:'Jodi',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:6,
      nama:'Joshua',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:7,
      nama:'Aldy',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:8,
      nama:'Sasha',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:9,
      nama:'Alex',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
    {
      id:10,
      nama:'Alifia',
      programStudi:'Informatika',
      jam:'10:50',
      avatar:'https://avatarfiles.alphacoders.com/946/94610.jpg'
    },
  ]

  const [mahasiswa, setMahasiswa]= useState(dataMahasiswa)

const renderItem=({item})=>{
  return(
    <View style={styles.item}>
      <Image source={{uri:item.avatar}} style={styles.image}/>
      <View style={{borderBottomColor:'#ccc', borderBottomWidth:1, flex:1}}>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text style={{fontWeight:'bold', fontSize:15}}>{item.nama}</Text>
          <Text>{item.jam}</Text>
        </View>
        <Text>{item.programStudi}</Text>
      </View>
    </View>
  )
}
  return (
    <View style={styles.container}>
      <FlatList
        data={mahasiswa}
        renderItem={renderItem}
        keyExtractor={(item)=>item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  item:{
    flexDirection:'row',
    alignItems:'center',
    padding:10
  },
  image:{
    width:35,
    height:35,
    resizeMode:'cover',
    borderRadius:35/2,
    marginRight:10
  }
});
